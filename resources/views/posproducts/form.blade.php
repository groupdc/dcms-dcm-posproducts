@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>POS Products</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/posproducts') !!}"><i class="far fa-shopping-cart"></i> POS Products</a></li>
            @if(isset($posproduct))
                <li class="active"><i class="far fa-pencil"></i>POS Product {{$posproduct->id}}</li>
            @else
                <li class="active"><i class="far fa-plus-circle"></i> Create POS Product</li>
            @endif
        </ol>
        @if(isset($posproduct))
            <p class="edit">Last edited by {{ $posproduct->admin }} on {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $posproduct->updated_at)->format('d-m-Y H:i:s') }}</p>
        @endif
    </div>

    <div class="main-content">
        <div class="row">

            @if(isset($posproduct))
                {!! Form::model($posproduct, array('route' => array('admin.posproducts.update', $posproduct->id), 'method' => 'PUT')) !!}
            @else
                {!! Form::open(array('url' => 'admin/posproducts')) !!}
            @endif

            <div class="col-md-12">

                <div class="main-content-tab tab-container">
                    @if (!is_array($categoryOptionValues) || count($categoryOptionValues)<=0 )    Please first create a <a
                            href="{!! URL::to('admin/posproducts/categories/create') !!}"> POS product category </a>  @else
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#data" role="tab" data-toggle="tab">Data</a></li>
                            <li><a href="#information" role="tab" data-toggle="tab">Information</a></li>
                        </ul>

                        <div class="tab-content">
                            @if($errors->any())
                                <div class="alert alert-danger">{!! Html::ul($errors->all()) !!}</div>
                            @endif
                            <div id="data" class="tab-pane active">
                                <!-- #data -->

                                <!-- #Code -->
                                <div class="form-group">
                                    {!! Form::label('code', 'Code') !!}
                                    {!! Form::text('code', old('code'), array('class' => 'form-control')) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('image', 'Image') !!}
                                    <div class="input-group">
                                        {!! Form::text('image', old('image'), array('class' => 'form-control')) !!}
                                        <span class="input-group-btn">{!! Form::button('Browse Server', array('class' => 'btn btn-primary browse-server' , 'id'=>'browse_image')) !!}</span>
                                    </div>
                                </div>

                                <!-- #Volume + unitclass (kg - l - g - ...) -->
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            {!! Form::label('volume', 'Volume') !!}
                                            {!! Form::text('volume', old('volume'), array('class' => 'form-control')) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('volume_unit_id', 'Unit') !!}
                                            {!! Form::select('volume_unit_id', (array(null=>'') + $volumeclasses), old('volume_unit_id'), array('class' => 'form-control')); !!}
                                        </div>
                                    </div>
                                </div>

                            <!-- #data -->
                            </div>

                            <div id="information" class="tab-pane">
                                <!-- #information -->
                                <div class="tab-container">

                                    @if(isset($languageinformation))

                                        <ul class="nav nav-tabs" role="tablist">
                                            @foreach($languageinformation as $key => $language)
                                                <li class="{!! ((intval(session('overrule_default_by_language_id')) == 0 && $key == 0) ? 'active' : '') !!}"><a href="{!! '#' . $language->language . '-' . $language->country !!}"
                                                                                                   role="tab" data-toggle="tab"><img
                                                                src="{!! asset('/packages/Dcms/Core/images/flag-' . strtolower($language->country) . '.png') !!}" width="16"
                                                                height="16"/> {!! $language->language_name !!}</a></li>
                                            @endforeach
                                        </ul>

                                        <div class="tab-content">
                                            @if(!isset($informationtemplate) || is_null($informationtemplate) )
                                                @include('dcmsposproducts::posproducts/templates/information', array('languageinformation'=>$languageinformation,'sortOptionValues'=>$sortOptionValues))
                                                @yield('information')
                                            @else
                                                @include($informationtemplate, array('languageinformation'=>$languageinformation,'sortOptionValues'=>$sortOptionValues))
                                                @yield('information')
                                            @endif
                                        </div>

                                    @endif

                                </div>
                                <!-- #information -->
                            </div>

                        </div><!-- end tab-content -->
                    @endif
                </div><!-- end main-content-tab -->
            </div><!-- end col-md-12 -->

            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>

            {!! Form::close() !!}

        </div><!-- end row -->
    </div><!-- end main-content -->

@stop

@section("script")

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckfinder/ckbrowser.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/packages/Dcms/Core/ckfinder/');

            //CKEditor
            $("textarea.ckeditor").ckeditor();

            //CKFinder
            $(".browse-server").click(function () {
                var returnid = $(this).attr("id").replace("browse_", "");
                BrowseServer('image:/', returnid);
            });

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            //ColorPicker
            $(".period-box .checkbox-group input").each(function () {
                $inputName = $(this).attr('name');
                $('[name="' + $inputName + '"]').paletteColorPicker({
                    clear_btn: null,
                    timeout: 700
                });
            });


            $('#information .input-group .information-id-reset').click(function () {
                $(this).closest(".tab-pane").find("input[id^='information_id']").val("");
                return false;
            });

            $("body").on("click", ".browse-server-files", function () {
                var returnid = $(this).attr("id").replace("browse_", "");
                BrowseServer('Files:/', returnid);
            });
        });
    </script>

@stop
