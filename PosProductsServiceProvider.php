<?php

namespace Dcms\PosProducts;

/**
 *
 * @author web <web@groupdc.be>
 */

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class PosProductsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__ . '/resources/views'), 'dcmsposproducts');
        $this->setupRoutes($this->app->router);
        // this  for conig
        $this->publishes([
            //  __DIR__.'/public/assets' => public_path('packages/Dcms/products'),
            __DIR__ . '/config/dcms_sidebar.php' => config_path('Dcms/POSProducts/dcms_sidebar.php'),
        ]);

        if (!is_null(config('dcms.posproducts'))) {
            $this->app['config']['dcms_sidebar'] = array_merge((array)$this->app["config"]["dcms_sidebar"], config('dcms.posproducts.dcms_sidebar'));
        }
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Dcms\PosProducts\Http\Controllers'], function ($router) {
            require __DIR__ . '/Http/routes.php';
        });
    }

    public function register()
    {
    }
}
