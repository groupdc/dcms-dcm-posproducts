<?php

namespace Dcms\PosProducts\Http\Controllers;

use App\Http\Controllers\Controller;

use Dcms\PosProducts\Models\POSCategory;
use Dcms\PosProducts\Models\POSInformation;
use Dcms\PosProducts\Models\POSProduct;
use Dcms\Products\Models\Product;
use Dcms\Products\Models\Information;
use Dcms\Products\Models\Attachment;
use Dcms\Products\Models\Price;

use Illuminate\Support\Str;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use DataTables;
use Auth;

class POSProductController extends Controller
{
    public $informatationColumNames = [];
    public $informatationColumNamesDefaults = []; // TO DO - the input on the information tab are array based
    public $productColumNames = [];
    public $productColumnNamesDefaults = []; // e.g. checkboxes left blank will result in NULL database value, if this is not what you want, you can set e.g. array('checkbox_name'=>'0');
    public $extendgeneralTemplate = "";
    public $informationTemplate = "";
    public $information_group_id = "";

    public function __construct()
    {
        $this->middleware('permission:posproducts-browse')->only('index');
        $this->middleware('permission:posproducts-add')->only(['create', 'store']);
        $this->middleware('permission:posproducts-edit')->only(['edit', 'update']);
        $this->middleware('permission:posproducts-delete')->only('destroy');

        $this->informationColumNames = [
            'title'                    => 'information_name'
            , 'description'            => 'information_description'
            , 'description_short'      => 'information_description_short'
            , 'sort_id'                => 'information_sort_id'
            , 'posproduct_category_id' => 'posinformation_category_id'
            , 'url_slug'               => 'information_name',
        ];

        $this->productColumNames = [
            'code'             => 'code'
            , 'image'          => 'image'
            , 'volume'         => 'volume'
            , 'volume_unit_id' => 'volume_unit_id',
        ];

        $this->extendgeneralTemplate = null;
        $this->informationTemplate = null;
        $this->information_group_id = null;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('dcmsposproducts::posproducts/index');
    }

    /**
     * get the data for DataTable JS plugin.
     *
     * @return Response
     */
    public function getDatatable()
    {
        return DataTables::queryBuilder(
            DB::connection("project")->table("posproducts")->select(
                "posproducts.id",
                "posproducts.code",
                "posproducts_to_products_information.posproduct_information_id as info_id",
                "title",
                DB::raw('CONCAT(volume, " ", volume_unit) AS volume'),
                (DB::connection("project")->raw("concat(\"<img src='/packages/Dcms/Core/images/flag-\", lcase(countries.country),\".png' > \") as country"))
            )
                ->leftJoin('posproducts_to_products_information', 'posproducts.id', '=', 'posproducts_to_products_information.posproduct_id')
                ->leftJoin('posproducts_information', 'posproducts_information.id', '=', 'posproducts_to_products_information.posproduct_information_id')
                ->leftJoin('posproducts_volume_units', 'posproducts.volume_unit_id', '=', 'posproducts_volume_units.id')
                ->leftJoin('languages', 'posproducts_information.language_id', '=', 'languages.id')
                ->leftJoin('countries', 'languages.country_id', '=', 'countries.id')
        )
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/posproducts/' . (isset($model->info_id) ? $model->info_id : $model->id) . '" accept-charset="UTF-8" class="pull-right"> <input name="_token" type="hidden" value="' . csrf_token() . '"> <input name="_method" type="hidden" value="DELETE">
                						<input type="hidden" name="table" value="' . (isset($model->info_id) ? "information" : "product") . '"/>
                						<input type="hidden" name="posproduct_id" value="' . $model->id . '"/>';

                if (Auth::user()->can('products-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/posproducts/' . $model->id . '/edit"><i class="far fa-pencil"></i></a>';
                }
                if (Auth::user()->can('products-delete')) {
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this POS product" onclick="if(!confirm(\'Are you sure to delete this item ? \')){return false;};"><i class="far fa-trash-alt"></i></button>';
                }
                $edit .= '</form>';

                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    //return the model/object (id, country, language) or an array e.g. array(language_id => language-COUNTRY)
    public function getCountriesLanguages($returnType = "array")
    {
        //RFC 3066
        $oCountriesLanguages = DB::connection("project")->select('SELECT id, country, language FROM languages ');

        if ($returnType === "model") {
            return $oCountriesLanguages;
        } else {
            $aCountryLanguage = [];

            if (!is_null($oCountriesLanguages) && count($oCountriesLanguages) > 0) {
                foreach ($oCountriesLanguages as $M) {
                    $aCountryLanguage[$M->id] = strtolower($M->language) . "-" . strtoupper($M->country);
                }
            }

            return $aCountryLanguage;
        }
    }

    //return the model/object or an array e.g. array(counrty_id => counryname)
    public function getCountries($returnType = "array")
    {
        $oCountries = DB::connection("project")->select('SELECT id, country_name FROM countries');
        if ($returnType === "model") {
            return $oCountries;
        } else {
            $aCountries = [];

            foreach ($oCountries as $c) {
                $aCountries[$c->id] = $c->country_name;
            }

            return $aCountries;
        }
    }

    public function getVolumesClasses($returnType = "array")
    {
        //volumeclasses
        //there is no model for VOLUMES so no eloquent querying here
        $oVolumeClasses = DB::connection("project")->select('SELECT id, volume_unit as volume FROM posproducts_volume_units ORDER BY 2');

        if ($returnType === "model") {
            return $oVolumeClasses;
        } else {
            //there was no support for the lists() method
            $aVolumesClasses = [];

            foreach ($oVolumeClasses as $v) {
                $aVolumesClasses[$v->id] = $v->volume;
            }

            return $aVolumesClasses;
        }
    }

    //return the model to fill the form
    public function getExtendedModel()
    {
        //do nothing sit back and make the extension hook up.
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $languageinformation = $this->getInformation();

        return View::make('dcmsposproducts::posproducts/form')
            ->with('languageinformation', $languageinformation)
            ->with('informationtemplate', $this->informationTemplate)//giving null will make a fallback to the default productinformation template on the package
            ->with('volumeclasses', $this->getVolumesClasses("array"))
            ->with('categoryOptionValues', POSCategory::OptionValueTreeArray(false))
            ->with('sortOptionValues', $this->getSortOptions($languageinformation, 1));
    }

    protected function validateProductForm()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = [
            'code' => 'required',
        ];
        $validator = Validator::make(request()->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()//to('admin/products/' . $id . '/edit')
            ->withErrors($validator)
                ->withInput();
        } else {
            return true;
        }
    }


    /**
     * Store the product based on a productid or a new product
     *
     * @return Product Object
     */
    protected function saveProductProperties($productid = null)
    {
        // do check if the given id is existing.
        if (!is_null($productid) && intval($productid) > 0) {
            $Product = POSProduct::find($productid);
        }

        if (!isset($Product) || is_null($Product)) {
            $Product = new POSProduct;
        }

        foreach ($this->productColumNames as $column => $inputname) {
            if (!request()->has($inputname) && array_key_exists($inputname, $this->productColumnNamesDefaults)) {
                $Product->$column = $this->productColumnNamesDefaults[$inputname];
            } else {
                $Product->$column = request()->get($inputname);
            }
        }

        $Product->save();
        $Product->information()->detach(); //detach any information setting, this will be set up using teh saveProductInformation() method

        return $Product;
    }

    /**
     * Save the ProductInformation to the given product (object)
     * this can be filtered by givin a single languageid - the filter helps for returning the model of the saved Information
     * by default you get back the last saved Information object
     * @return Product Object
     */
    protected function saveProductInformation(POSProduct $Product, $givenlanguage_id = null)
    {
        $input = request()->all();

        $pInformation = null; //pInformation = object product Information

        if (isset($input["information_language_id"]) && count($input["information_language_id"]) > 0) {
            foreach ($input["information_language_id"] as $i => $language_id) {
                if ((is_null($givenlanguage_id) || ($language_id == $givenlanguage_id)) && (strlen(trim($input[$this->informationColumNames['title']][$i])) > 0)) {
                    $pInformation = null; //reset when in a loop
                    $newInformation = true;

                    if (intval($input["information_id"][$i]) > 0) {
                        $pInformation = POSInformation::find($input["information_id"][$i]);
                    }

                    if (!isset($pInformation) || is_null($pInformation)) {
                        $pInformation = new POSInformation;
                    } else {
                        $newInformation = false;
                    }

                    $oldSortID = null;
                    if ($newInformation == false && !is_null($pInformation->sort_id) && intval($pInformation->sort_id) > 0) {
                        $oldSortID = intval($pInformation->sort_id);
                    }

                    foreach ($this->informationColumNames as $column => $inputname) {
                        if (isset($input[$inputname][$i])) {
                            $pInformation->$column = $input[$inputname][$i];
                        } else {
                            $pInformation->$column = null;
                        }
                    }

                    $pInformation->language_id = $input["information_language_id"][$i];//$language_id;
                    $pInformation->posproduct_category_id = ($input[$this->informationColumNames['posproduct_category_id']][$i] == 0 ? null : $input[$this->informationColumNames['posproduct_category_id']][$i]);
                    $pInformation->url_slug = Str::slug($input[$this->informationColumNames['url_slug']][$i]);

                    $pInformation->save();
                    $Product->information()->attach($pInformation->id);

                    $sort_incrementstatus = "0"; //the default
                    if (is_null($oldSortID) || $oldSortID == 0) {
                        //update all where sortid >= input::sortid
                        $updateInformations = POSInformation::where('language_id', '=', $language_id)->where('sort_id', '>=', $input[$this->informationColumNames['sort_id']][$i])->where('id', '<>', $pInformation->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "+1";
                    } elseif ($oldSortID > $input[$this->informationColumNames['sort_id']][$i]) {
                        $updateInformations = POSInformation::where('language_id', '=', $language_id)->where('sort_id', '>=', $input[$this->informationColumNames['sort_id']][$i])->where('sort_id', '<', $oldSortID)->where('id', '<>', $pInformation->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "+1";
                    } elseif ($oldSortID < $input[$this->informationColumNames['sort_id']][$i]) {
                        $updateInformations = POSInformation::where('language_id', '=', $language_id)->where('sort_id', '>', $oldSortID)->where('sort_id', '<=', $input[$this->informationColumNames['sort_id']][$i])->where('id', '<>', $pInformation->id)->get(['id', 'sort_id']);
                        $sort_incrementstatus = "-1";
                    }

                    if ($sort_incrementstatus <> "0") {
                        if (isset($updateInformations) && count($updateInformations) > 0) {
                            foreach ($updateInformations as $uInformation) {
                                if ($sort_incrementstatus == "+1") {
                                    $uInformation->sort_id = intval($uInformation->sort_id) + 1;
                                    $uInformation->save();
                                } elseif ($sort_incrementstatus == "-1") {
                                    $uInformation->sort_id = intval($uInformation->sort_id) - 1;
                                    $uInformation->save();
                                }
                            }//end foreach($updateInformations as $Information)
                        }//end 	if (count($updateInformations)>0)
                    }//$sort_incrementstatus <> "0"
                }//end if($language_id ==$language_id
            }//foreach($input["information_language_id"] as $i => $language_id)
        }//if (isset($input["information_language_id"]) && count($input["information_language_id"])>0)

        // based on the Product->id we can find the attached info's
        // if the attached info's contain a information_group_id we can use this for the rest of the info's
        // otherwise create a new group_id
        $information_group_id = 0; //define var
        $ProductInformationObject = $Product->information; //set it to a var, so the query will run once in this part.

        if ($ProductInformationObject->count() > 0) {
            foreach ($ProductInformationObject as $I) {
                if (intval($I->information_group_id) > 0) {
                    if (!is_null($I->information_group_id) && intval($I->information_group_id) > 0) {
                        $information_group_id = intval($I->information_group_id);
                        break;
                    }
                }
            }
            if ($information_group_id <= 0) {
                $igi = POSInformation::orderBy('information_group_id', 'desc')->take(1)->get();
                $information_group_id = intval($igi[0]->information_group_id) + 1;
            }

            $this->information_group_id = $information_group_id;

            foreach ($ProductInformationObject as $I) {
                $I = POSInformation::find($I->id);
                $I->information_group_id = $information_group_id;
                $I->save();
            }
        }

        return $pInformation;
    }

    public function getInformation($id = null)
    {
        if (is_null($id)) {
            return DB::connection("project")->table("languages")->select((DB::connection("project")->raw("'' as title, '' as description, '' as description_short,  NULL as sort_id, (select max(sort_id) from products_information where language_id = languages.id) as maxsort, '' as information_id, '' as id , '' as posproduct_category_id")), "id as language_id", "language", "language_name", "country")->get();
        } else {
            return DB::connection("project")->select('
														SELECT posproducts_information.language_id ,sort_id, (select max(sort_id) from posproducts_information as X  where X.language_id = posproducts_information.language_id) as maxsort, language, language_name, country, posproducts_information.title, posproducts_information.description, posproducts_information.description_short, posproducts_information.id as information_id, posproduct_category_id
														FROM  posproducts
														INNER JOIN posproducts_to_products_information on posproducts.id = posproducts_to_products_information.posproduct_id
														INNER JOIN posproducts_information on posproducts_to_products_information.posproduct_information_id = posproducts_information.id
														INNER JOIN languages on languages.id = posproducts_information.language_id
														WHERE posproducts.id = ?

														UNION
														SELECT languages.id as language_id , 0, (select max(sort_id) from posproducts_information where language_id = languages.id), language, language_name, country, \'\' as title, \'\' as description, \'\' as description_short, \'\' as information_id , \'\'
														FROM languages
														WHERE id NOT IN (SELECT language_id FROM posproducts_information WHERE id IN (SELECT posproduct_information_id FROM posproducts_to_products_information WHERE posproduct_id = ?)) ORDER BY 1 ', [$id, $id]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $posproduct = POSProduct::find($id);

        $languageinformation = $this->getInformation($id);

        return View::make('dcmsposproducts::posproducts/form')
            ->with('posproduct', $posproduct)
            ->with('informationtemplate', $this->informationTemplate)//giving null will make a fallback to the default productinformation template on the package
            ->with('languageinformation', $languageinformation)
            ->with('volumeclasses', $this->getVolumesClasses("array"))
            ->with('categoryOptionValues', POSCategory::OptionValueTreeArray(false))
            ->with('sortOptionValues', $this->getSortOptions($languageinformation));
    }

    public function getSortOptions($model, $setExtra = 0)
    {
        $SortOptions = [];
        foreach ($model as $M) {
            $increment = 0;
            if ($setExtra > 0) {
                $increment = $setExtra;
            }
            if (intval($M->information_id) <= 0 && !is_null($M->maxsort)) {
                $increment = 1;
            }

            $maxSortID = $M->maxsort;
            if (is_null($maxSortID)) {
                $maxSortID = 1;
            }

            for ($i = 1; $i <= ($maxSortID + $increment); $i++) {
                $SortOptions[$M->language_id][$i] = $i;
            }
        }

        return $SortOptions;
    }


    /**
     * copy the model
     *
     * @param  int $product_id
     * @param  int $country_id //helps limiting the prices copy - we don't need the copied product in all countries..
     *
     * @return Response
     */
    public function copy($product_id, $country_id = 0)
    {
        //COPY THE PRODUCT
        $Newproduct = Product::find($product_id)->replicate();
        $Newproduct->save();

        //COPY THE Information
        $ProductsInformation = Product::with('Information')->where('id', '=', $product_id)->get();

        if (!is_null($ProductsInformation)) {
            foreach ($ProductsInformation as $P) {
                foreach ($P->Information as $pInformation) {
                    $Newproduct->information()->attach($pInformation->id);
                }
            }
        }

        //COPY THE Attachment
        $Attachements = Attachment::where('product_id', '=', $product_id)->get();
        if (!is_null($Attachements)) {
            foreach ($Attachements as $A) {
                $newAttachement = $A->replicate();
                $newAttachement->product_id = $Newproduct->id;
                $newAttachement->save();
            }
        }

        //COPY THE Prices
        $Prices = Price::where('product_id', '=', $product_id)->where('country_id', '=', $country_id)->get();
        if (!is_null($Prices)) {
            foreach ($Prices as $P) {
                $newPrice = $P->replicate();
                $newPrice->product_id = $Newproduct->id;
                $newPrice->save();
            }
        }

        return Redirect::to('admin/products');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->validateProductForm() === true) {
            $Product = $this->saveProductProperties();
            $this->saveProductInformation($Product);

            Session::flash('message', 'Successfully created Product!');

            return Redirect::to('admin/posproducts');
        } else {
            return $this->validateProductForm();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        if ($this->validateProductForm() === true) {
            $Product = $this->saveProductProperties($id);
            $this->saveProductInformation($Product);

            Session::flash('message', 'Successfully updated Product!');

            return Redirect::to('admin/posproducts');
        } else {
            return $this->validateProductForm();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if (request()->get('table') == 'product') {
            Product::find($id)->delete();
            Price::where("product_id", "=", $id)->delete();
        } else {
            $Information = Information::find($id);
            $Information->products()->detach(request()->get("product_id"));
        }

        Session::flash('message', 'Successfully deleted the Product!');

        return Redirect::to('admin/posproducts');
    }
}
