<?php


Route::group(['middleware' => ['web']], function () {
    Route::group(["prefix" => "admin", "as" => "admin."], function () {
        Route::group(['middleware' => 'auth:dcms'], function () {

            //POSPRODUCTS
            Route::group(["prefix" => "posproducts", "as" => "posproducts."], function () {

                //CATEGORIES
                Route::group(["prefix" => "categories", "as" => "categories."], function () {
                    Route::get("{id}/copy", ["as" => "{id}.copy", "uses" => "POSCategoryController@copy"]);
                    Route::any("api/table", ["as" => "api.table", "uses" => "POSCategoryController@getDatatable"]);
                    Route::any("rebuildTree", ["as" => "rebuildTree", "uses" => "POSCategoryController@rebuildTree"]);
                });
                Route::resource("categories", "POSCategoryController");

                //INFORMATION
                Route::group(["prefix" => "information", "as" => "information."], function () {
                    Route::any("api/table", ["as" => "api.table", "uses" => "POSInformationController@getDatatable"]);
                    Route::any("api/articlerelationtable/{article_id?}", ["as" => "api.articlerelationtable", "uses" => "POSInformationController@getArticleRelationTable"]);
                    Route::any("api/plantrelationtable/{plant_id?}", ["as" => "api.plantrelationtable", "uses" => "POSInformationController@getPlantRelationTable"]);
                });

                //API
                Route::group(array("prefix" => "api","as"=>"api."), function () {
                    Route::any("table", ["as" => "table", "uses" => "POSProductController@getDatatable"]);
                });

                Route::resource("information", "POSInformationController");
            });

            Route::resource("posproducts", "POSProductController");
        });
    });
});
