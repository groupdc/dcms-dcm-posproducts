<?php

namespace Dcms\PosProducts\Models;

use Dcms\Advices\Models\Condition;
use Dcms\Core\Models\EloquentDefaults;

class POSInformation extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "posproducts_information";
    protected $fillable = array('language_id', 'title', 'description');

    protected function setPrimaryKey($key)
    {
        $this->primaryKey = $key;
    }

    public function posproducts()
    {
        return $this->belongsToMany('\Dcms\PosProducts\Models\POSProduct', 'posproducts_to_products_information', 'posproduct_information_id', 'posproduct_id')->withTimestamps();
    }

    public function posproductcategory()
    {
        return $this->belongsTo('\Dcms\PosProducts\Models\POSCategory', 'posproduct_category_id', 'id');
    }

    public function otherlanguages()
    {
        return $this->hasMany('\Dcms\PosProducts\Models\POSInformation', 'information_group_id', 'information_group_id')->whereNotNull('information_group_id');
    }
}
