<?php

namespace Dcms\PosProducts\Models;

use Dcms\Core\Models\EloquentDefaults;

use Auth;

class POSProduct extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table  = "posproducts";

    public function information()
    {
        return $this->belongsToMany('\Dcms\PosProducts\Models\POSInformation', 'posproducts_to_products_information', 'posproduct_id', 'posproduct_information_id')->withTimestamps();
    }
}
