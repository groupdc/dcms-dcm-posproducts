<?php

namespace Dcms\PosProducts\Models;

use DB;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class POSCategory extends Model
{
    use NodeTrait;

    protected $connection = 'project';
    protected $table  = "posproducts_categories_language";
    protected $fillable = array('language_id', 'title');

    public function getLftName()
    {
        return 'lft';
    }

    public function getRgtName()
    {
        return 'rgt';
    }

    public static function OptionValueTreeArray($enableEmpty = false, $columns = array('*'), $columnMapper = array("id","title","language_id"))
    {
        $PageObj = DB::connection('project')
                        ->table('posproducts_categories_language as node')
                        ->select(
                            (DB::connection("project")->raw("CONCAT( REPEAT( '-', node.depth ), node.title) AS title")),
                            "node.id",
                            "node.parent_id",
                            "node.language_id",
                            "node.depth",
                            (DB::connection("project")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".svg\' style=\'width:16px; height:auto;\'>") as regio'))
                                )
                        ->leftJoin('languages', 'node.language_id', '=', 'languages.id')
                        ->orderBy('node.lft')
                        ->get();

        $OptionValueArray = array();

        if (count($PageObj)>0) {
            foreach ($PageObj as $lang) {
                if (array_key_exists($lang->language_id, $OptionValueArray)== false) {
                    $OptionValueArray[$lang->language_id] = array();
                }

                //we  make an array with array[languageid][maincategoryid] = translated category;
                $columnMapper_zero = $columnMapper[0];
                $columnMapper_one = $columnMapper[1];
                $OptionValueArray[$lang->language_id][$lang->$columnMapper_zero]=/*str_repeat('-',$lang->level).' '.*/$lang->$columnMapper_one;
            }
        } elseif ($enableEmpty === true) {
            $Languages = Language::all();

            foreach ($Languages as $Lang) {
                $OptionValueArray[$Lang->id][1] = "- ROOT -";
            }
        }
        return $OptionValueArray;
    }

    public function saveDepth(){
        $this->depth = self::withDepth()->find($this->id)->depth;
        $this->save();
    }
}
