<?php

return [
    "POS Products" => [
        "icon"  => "fa-shopping-cart",
        "links" => [
            ["route" => "admin/posproducts", "label" => "POS Products", "permission" => 'posproducts'],
//            ["route" => "admin/posproducts/information", "label" => "Information", "permission" => 'posproducts'],
            ["route" => "admin/posproducts/categories", "label" => "POS Categories", "permission" => 'posproducts'],
        ],
    ],
];
