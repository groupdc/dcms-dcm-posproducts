/*
Navicat MySQL Data Transfer

Source Server         : local_wamp
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : pos

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-04-26 11:08:06
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `languages`
-- ----------------------------
/*
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) unsigned DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `language_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regional` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_langcountryid` (`country_id`),
  CONSTRAINT `FK_langcountryid` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('1', '1', 'nl', 'Nederlands', 'BE', 'nl_BE', null, '0000-00-00 00:00:00', '2015-01-05 13:02:08');
INSERT INTO `languages` VALUES ('2', '1', 'fr', 'Frans', 'BE', 'fr_BE', null, '0000-00-00 00:00:00', '2015-01-05 13:02:02');
INSERT INTO `languages` VALUES ('3', '2', 'nl', 'Nederlands', 'NL', 'nl_NL', null, '2014-07-16 10:45:19', '2015-01-05 13:02:16');
INSERT INTO `languages` VALUES ('6', '3', 'fr', 'Français', 'FR', 'fr_FR', null, '2015-01-05 11:58:16', '2015-01-05 13:01:29');
INSERT INTO `languages` VALUES ('7', '4', 'de', 'Deutsch', 'DE', 'de_DE', null, '2015-01-05 11:58:23', '2015-01-05 13:01:37');
INSERT INTO `languages` VALUES ('8', '5', 'es', 'Spanish', 'ES', 'es_ES', 'bartr', '2015-06-19 16:06:05', '2015-06-19 16:06:05');
INSERT INTO `languages` VALUES ('9', '6', 'it', 'Italian', 'IT', 'it_IT', 'bartr', '2015-06-19 16:07:32', '2015-06-19 16:07:32');
INSERT INTO `languages` VALUES ('10', '8', 'en', 'English', 'US', 'en_US', 'bartr', '2015-06-19 16:07:43', '2015-06-19 16:07:43');
INSERT INTO `languages` VALUES ('11', '7', 'en', 'English', 'INT', 'en_GB', 'bartr', '2015-06-19 16:07:55', '2015-06-19 16:07:55');
*/
-- ----------------------------
-- Table structure for `posproducts`
-- ----------------------------
DROP TABLE IF EXISTS `posproducts`;
CREATE TABLE `posproducts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volume` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volume_unit_id` int(11) unsigned DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  `updated_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_pos_volume_unit_id` (`volume_unit_id`) USING BTREE,
  CONSTRAINT `FK_pos_prdvolume_unit_id` FOREIGN KEY (`volume_unit_id`) REFERENCES `products_volume_units` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1519 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of posproducts
-- ----------------------------

-- ----------------------------
-- Table structure for `posproducts_categories_language`
-- ----------------------------
DROP TABLE IF EXISTS `posproducts_categories_language`;
CREATE TABLE `posproducts_categories_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rgt` int(11) DEFAULT NULL,
  `depth` int(11) DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `sort_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `canonical_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  `updated_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_pos_categorylanguage` (`language_id`),
  KEY `pos_parent` (`parent_id`),
  CONSTRAINT `pos_parent` FOREIGN KEY (`parent_id`) REFERENCES `posproducts_categories_language` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `posproducts_categories_language_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=426 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of posproducts_categories_language
-- ----------------------------

-- ----------------------------
-- Table structure for `posproducts_information`
-- ----------------------------
DROP TABLE IF EXISTS `posproducts_information`;
CREATE TABLE `posproducts_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `information_group_id` int(11) unsigned DEFAULT NULL,
  `language_id` int(11) unsigned DEFAULT '1',
  `posproduct_category_id` int(11) unsigned DEFAULT NULL,
  `sort_id` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_short` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `canonical_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_informationgroupid_language` (`information_group_id`,`language_id`),
  KEY `FK_productlanguage` (`language_id`),
  KEY `FK_posproductcategory` (`posproduct_category_id`),
  KEY `information_group_id` (`information_group_id`),
  FULLTEXT KEY `ProductSearchHelpertitle` (`title`,`description`),
  CONSTRAINT `FK_posproductcategory` FOREIGN KEY (`posproduct_category_id`) REFERENCES `posproducts_categories_language` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `FK_pos_productlanguage` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1720 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of posproducts_information
-- ----------------------------

-- ----------------------------
-- Table structure for `posproducts_to_products_information`
-- ----------------------------
DROP TABLE IF EXISTS `posproducts_to_products_information`;
CREATE TABLE `posproducts_to_products_information` (
  `posproduct_id` int(11) unsigned NOT NULL,
  `posproduct_information_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  `updated_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  PRIMARY KEY (`posproduct_id`,`posproduct_information_id`),
  KEY `FK_prdtoprdinfo_infoid` (`posproduct_information_id`),
  CONSTRAINT `FK_pos_prdtoprdinfo_infoid` FOREIGN KEY (`posproduct_information_id`) REFERENCES `posproducts_information` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_pos_prdtoprdinfo_prdid` FOREIGN KEY (`posproduct_id`) REFERENCES `posproducts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of posproducts_to_products_information
-- ----------------------------

-- ----------------------------
-- Table structure for `posproducts_volume_units`
-- ----------------------------
DROP TABLE IF EXISTS `posproducts_volume_units`;
CREATE TABLE `posproducts_volume_units` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `volume_unit` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  `updated_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of posproducts_volume_units
-- ----------------------------
INSERT INTO `posproducts_volume_units` VALUES ('1', 'kg', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('2', 'g', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('3', 'L', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('4', 'set', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('5', 'analyse', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('7', 'piece', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('8', 'kit', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('9', 'vrac', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('10', 'big bag', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('11', 'bar', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('12', 'ml', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('13', 'cm', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('14', 'bottle', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('15', 'bucket', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('16', 'box', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('17', 'pots', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('18', 'm²', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('19', 'trap + glue boards', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('20', 'capsules', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('21', 'traps', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('22', 'Larvae', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('23', 'Cocoon', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('24', 'Predatory mites', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('25', 'Eels', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units` VALUES ('26', 'm', '1971-01-01 00:00:00', '1971-01-01 00:00:00');

-- ----------------------------
-- Table structure for `posproducts_volume_units_language`
-- ----------------------------
DROP TABLE IF EXISTS `posproducts_volume_units_language`;
CREATE TABLE `posproducts_volume_units_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `posvolume_units_id` int(11) unsigned NOT NULL,
  `language_id` int(11) unsigned DEFAULT NULL,
  `volume_unit` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `volume_unit_long` varchar(70) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multiple_from_base` int(11) DEFAULT '0' COMMENT 'base,ones,units, "fingers" ; base =1 kilo = 1000',
  `created_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  `updated_at` timestamp NULL DEFAULT '1971-01-01 00:00:00',
  PRIMARY KEY (`id`),
  KEY `FK_volunit` (`posvolume_units_id`),
  KEY `FK_volunitlang` (`language_id`),
  CONSTRAINT `FK_pos_volunit` FOREIGN KEY (`posvolume_units_id`) REFERENCES `posproducts_volume_units` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_pos_volunitlang` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of posproducts_volume_units_language
-- ----------------------------
INSERT INTO `posproducts_volume_units_language` VALUES ('1', '1', '1', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('2', '1', '2', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('3', '2', '1', 'g', 'gram', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('4', '2', '2', 'g', 'gram', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('5', '3', '1', 'L', 'liter', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('6', '3', '2', 'L', 'litre', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('7', '4', '1', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('8', '4', '2', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('9', '5', '1', 'analyse', 'analyse', '1', '1971-01-01 00:00:00', '2015-04-21 09:28:10');
INSERT INTO `posproducts_volume_units_language` VALUES ('10', '5', '2', 'analyse', 'analyse', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('11', '7', '1', 'stuk', 'stuk', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('12', '7', '2', 'pièce', 'pièce', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('13', '8', '1', 'kit', 'kit', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('14', '8', '2', 'kit', 'kit', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('15', '9', '1', 'vrac', 'vrac', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('16', '9', '2', 'vrac', 'vrac', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('17', '10', '1', 'big bag', 'big bag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('18', '10', '2', 'big bag', 'big bag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('19', '11', '1', 'staafjes', 'staafjes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('20', '11', '2', 'bâtonnets', 'bâtonnets', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('21', '1', '7', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('22', '1', '6', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('24', '2', '7', 'g', 'gram', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('25', '2', '6', 'g', 'gram', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('27', '3', '7', 'L', 'liter', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('28', '3', '6', 'L', 'litre', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('30', '4', '7', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('31', '4', '6', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('32', '5', '7', 'analyse', 'analyse', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('33', '7', '7', 'stück', 'stück', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('34', '8', '7', 'Bausatz', 'Bausatz', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('35', '9', '7', 'vrac', 'vrac', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('36', '10', '7', 'big bag', 'big bag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('37', '1', '8', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('38', '2', '8', 'g', 'gram', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('39', '3', '8', 'L', 'liter', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('40', '4', '8', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('41', '5', '8', 'analyse', 'analyse', '1', '1971-01-01 00:00:00', '2015-04-21 09:28:10');
INSERT INTO `posproducts_volume_units_language` VALUES ('42', '7', '8', 'stuk', 'stuk', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('43', '8', '8', 'kit', 'kit', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('44', '9', '8', 'vrac', 'vrac', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('45', '10', '8', 'big bag', 'big bag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('46', '11', '8', 'staafjes', 'staafjes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('47', '1', '9', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('48', '2', '9', 'g', 'gram', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('49', '3', '9', 'L', 'liter', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('50', '4', '9', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('51', '5', '9', 'analyse', 'analyse', '1', '1971-01-01 00:00:00', '2015-04-21 09:28:10');
INSERT INTO `posproducts_volume_units_language` VALUES ('52', '7', '9', 'stuk', 'stuk', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('53', '8', '9', 'kit', 'kit', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('54', '9', '9', 'vrac', 'vrac', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('55', '10', '9', 'big bag', 'big bag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('56', '11', '9', 'staafjes', 'staafjes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('57', '1', '10', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('58', '2', '10', 'g', 'gram', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('59', '3', '10', 'L', 'liter', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('60', '4', '10', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('61', '5', '10', 'analyse', 'analyse', '1', '1971-01-01 00:00:00', '2015-04-21 09:28:10');
INSERT INTO `posproducts_volume_units_language` VALUES ('62', '7', '10', 'stuk', 'stuk', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('63', '8', '10', 'kit', 'kit', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('64', '9', '10', 'vrac', 'vrac', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('65', '10', '10', 'big bag', 'big bag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('66', '11', '10', 'staafjes', 'staafjes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('67', '1', '11', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('68', '2', '11', 'g', 'gram', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('69', '3', '11', 'L', 'liter', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('70', '4', '11', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('71', '5', '11', 'analyse', 'analyse', '1', '1971-01-01 00:00:00', '2015-04-21 09:28:10');
INSERT INTO `posproducts_volume_units_language` VALUES ('72', '7', '11', 'stuk', 'stuk', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('73', '8', '11', 'kit', 'kit', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('74', '9', '11', 'vrac', 'vrac', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('75', '10', '11', 'big bag', 'big bag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('76', '11', '11', 'staafjes', 'staafjes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('77', '1', '3', 'kg', 'kilogram', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('78', '2', '3', 'g', 'g', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('79', '3', '3', 'L', 'l', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('80', '4', '3', 'set', 'set', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('81', '5', '3', 'analyse', 'analyse', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('82', '7', '3', 'stuk', 'stuk', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('83', '8', '3', 'kit', 'kit', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('84', '9', '3', 'vrac', 'vrag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('85', '10', '3', 'big bag', 'big bag', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('86', '11', '3', 'staafjes', 'staafjes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('87', '12', '1', 'ml', 'ml', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('88', '12', '2', 'ml', 'ml', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('89', '12', '3', 'ml', 'ml', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('90', '13', '1', 'cm', 'cm', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('91', '13', '2', 'cm', 'cm', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('92', '13', '3', 'cm', 'cm', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('93', '14', '1', 'bus', 'bus', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('94', '14', '2', 'bidons', 'bidons', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('95', '14', '3', 'bus', 'bus', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('96', '15', '1', 'emmer', 'emmer', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('97', '15', '2', 'seau', 'seau', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('98', '15', '3', 'emmer', 'emmer', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('99', '16', '1', 'dozen', 'dozen', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('100', '16', '2', 'boîtes', 'boîtes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('101', '16', '3', 'dozen', 'dozen', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('102', '17', '1', 'potjes', 'potjes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('103', '17', '2', 'pots', 'pots', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('104', '17', '3', 'potjes', 'potjes', '1', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('105', '18', '1', 'm²', 'm²', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('106', '18', '2', 'm²', 'm²', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('107', '18', '3', 'm²', 'm²', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('110', '18', '6', 'm²', 'm²', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('111', '18', '7', 'm²', 'm²', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('112', '18', '8', 'm²', 'm²', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('113', '19', '1', 'val + lijmborden', 'val + lijmborden', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('114', '19', '2', 'piège + plaques engluées', 'piège + plaques engluées', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('115', '19', '3', 'val + lijmborden', 'val + lijmborden', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('116', '19', '6', 'piège + plaques engluées', 'piège + plaques engluées', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('117', '20', '1', 'capsules', 'capsules', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('118', '20', '2', 'capsules', 'capsules', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('119', '20', '3', 'capsules', 'capsules', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('120', '20', '6', 'capsules', 'capsules', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('121', '21', '1', 'platen', 'platen', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('122', '21', '2', 'pièges', 'pièges', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('123', '21', '3', 'platen', 'platen', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('124', '21', '6', 'pièges', 'pièges', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('125', '22', '1', 'larven', 'larven', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('126', '22', '2', 'larves', 'larves', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('127', '22', '3', 'larven', 'larven', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('128', '23', '1', 'poppen', 'poppen', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('129', '23', '2', 'pupes', 'pupes', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('130', '23', '3', 'poppen', 'poppen', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('131', '24', '1', 'roofmijten', 'roofmijten', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('132', '24', '2', 'acariens prédateurs', 'acariens prédateurs', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('133', '24', '3', 'roofmijten', 'roofmijten', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('134', '25', '1', 'aaltjes', 'aaltjes', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('135', '25', '2', 'nématodes', 'nématodes', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('136', '25', '3', 'aaltjes', 'aaltjes', '0', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('137', '26', '1', 'm', 'meter', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('138', '26', '2', 'm', 'mètre', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
INSERT INTO `posproducts_volume_units_language` VALUES ('139', '26', '3', 'm', 'meter', '1000', '1971-01-01 00:00:00', '1971-01-01 00:00:00');
DELIMITER ;;
CREATE TRIGGER `before_insert_pos_products` BEFORE INSERT ON `posproducts` FOR EACH ROW BEGIN
    SET new.uuid = uuid();
END
;;
DELIMITER ;
